###out:src/main/java/${config.packageName?replace(".","/")}/${config.entityPackageName}/${root.beanName}.java
package ${config.packageName}.${config.entityPackageName};

import java.util.Date;
import java.math.BigDecimal;
import com.zzb.framework.common.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
* ${root.beanName} 实体类
* by ${root.systemUserName} ${root.currentTime?string('yyyy-MM-dd HH:mm:ss')}
*/
@Data
@TableName("${root.tableName}")
public class ${root.beanName} BaseEntity<String> {
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    <#list root.tableColumns as column><#if column.columnName != 'id' && column.columnName != 'create_time' && column.columnName != 'creator'  && column.columnName != 'update_time'  && column.columnName != 'updater'  && column.columnName != 'activate'>
        @TableField(value = "${column.columnName}")
        private ${column.propertyType} ${column.propertyName}; //${column.columnComment}
    </#if></#list>

}
