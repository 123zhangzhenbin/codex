package com.zzb.codegenerator;
import java.io.*;

/**
 * Created by zhangzhenbin on 18-4-4.
 */
public class JoinFile {
    public static void main(String[] args) throws IOException {
        File file = new File("/home/zhangzhenbin/Documents/projects/codex/testout/doc");
        File[] files = file.listFiles();
        File joinFile = new File("/home/zhangzhenbin/Documents/projects/codex/testout/joinFiles/all-ddl.sql");
        BufferedWriter bw = new BufferedWriter(new FileWriter(joinFile));
        System.out.println("files num :" + files.length);
        for (File f : files) {
            BufferedReader br = new BufferedReader(new FileReader(f));
            br.lines().forEach(s -> {
                try {
                    System.out.println(s);
                    bw.append(s).append("\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            br.close();
        }
        bw.flush();
    }
}
