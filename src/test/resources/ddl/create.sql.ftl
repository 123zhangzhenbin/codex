###out:doc/${root.tableName}.sql
--  ${root.tableName}
create table ${root.tableName}(
kafka_partition int,
kafka_offset bigint,
kafka_topic string,
<#list root.tableColumns as column><#if column.columnName?lower_case != 'rowversioncol'>${column.columnName?lower_case} <#if column.propertyType == 'BigDecimal' || column.propertyType == 'Float' || column.propertyType == 'Double'>double<#elseif column.propertyType == 'String'>string<#elseif column.propertyType == 'Long'>bigint<#elseif column.propertyType == 'Integer'>int<#elseif column.propertyType == 'Date'>timestamp<#elseif column.propertyType == 'Boolean'>int</#if> ,</#if>
</#list>
rowversioncol string,
dml_operation int,
PRIMARY KEY (kafka_partition,kafka_offset) )
PARTITION BY HASH (kafka_partition,kafka_offset) PARTITIONS 16 STORED AS KUDU;
