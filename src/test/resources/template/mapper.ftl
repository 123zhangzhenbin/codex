###out:src/main/resources/mapper/Cdc${root.beanName}Mapper.xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.tuandai.dao.Cdc${root.beanName}DAO">

    <select id="listInfo" resultType="com.tuandai.domain.CdcInfo">
        <![CDATA[
        SELECT ${r'${searchDTO.topCondition}'}
        __$operation as operation, __$start_lsn as lsn, __$seqval as seqval, Id as id, AddDate as addDate,
        ]]>
        <if test="searchDTO.queryDate != null and searchDTO.queryDate == true">case __$operation when 2 then Adddate else null end as addDate,</if>
        <![CDATA[
        case __$operation when 2 then
        'INSERT INTO _REPLACE_(<#list root.tableColumns as column>${column.columnName},</#list> cdcName) VALUES(' + <#list root.tableColumns as column><#if column.columnType == 'VARCHAR' || column.columnType == 'CHAR' || column.columnType == 'UNIQUEIDENTIFIER' || column.columnType == 'TEXT'>
            isnull(''''+convert(varchar(100),${column.columnName})+'''','NULL')+<#elseif column.columnType =='DATETIME' || column.columnType =='DATE' || column.columnType =='TIMESTAMP' || column.columnType =='DATETIME2'>
            isnull(''''+convert(varchar(100),${column.columnName},121)+'''','NULL')+<#else>
            isnull(convert(varchar(100),${column.columnName}),'NULL')+</#if>',' +</#list>
            '''${r'${searchDTO.cdcName}'}'');'
        when 4 then
        'UPDATE _REPLACE_ SET '+ <#list root.tableColumns as column><#if column.primaryKey == false><#if column.columnType == 'VARCHAR' || column.columnType == 'CHAR' || column.columnType == 'UNIQUEIDENTIFIER' || column.columnType == 'TEXT'>
            '${column.columnName}='+isnull(''''+convert(varchar(100),${column.columnName})+'''','NULL')+<#elseif column.columnType =='DATETIME' || column.columnType =='DATE' || column.columnType =='TIMESTAMP' || column.columnType =='DATETIME2'>
            '${column.columnName}='+isnull(''''+convert(varchar(100),${column.columnName},121)+'''','NULL')+<#else>
            '${column.columnName}='+isnull(convert(varchar(100),${column.columnName}),'NULL')+</#if>',' + </#if></#list>
            'cdcName=''${r'${searchDTO.cdcName}'}''' +
            'WHERE ${root.primaryKeyColumn.columnName}=' +''''+convert(varchar(100),${root.primaryKeyColumn.columnName})+''''+';'
        when 1 then
        'DELETE FROM _REPLACE_ WHERE ${root.primaryKeyColumn.columnName}='+''''+convert(varchar(100),${root.primaryKeyColumn.columnName})+''''+';' end as HandleSQL
        from cdc.dbo_${root.tableName}_CT with(nolock)
        where  ${r'${searchDTO.condition}'} and __$operation<>3
        order by __$start_lsn asc,__$seqval asc
        ]]>
    </select>

    <select id="getFirstLsn" resultType="java.lang.String">
        <![CDATA[
        SELECT top 1 __$start_lsn as lsn
        from cdc.dbo_${root.tableName}_CT with(nolock)
        where  __$operation<>3
        order by __$start_lsn asc,__$seqval asc
        ]]>
    </select>

    <select id="countByLsn" resultType="int">
        <![CDATA[
        SELECT COUNT(1)
        FROM cdc.dbo_${root.tableName}_CT (NOLOCK)
        WHERE __$start_lsn = ${r'${lsn}'} and __$operation<>3
        ]]>
    </select>

</mapper>