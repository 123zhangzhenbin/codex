###out:src/main/java/com/tuandai/common/taskSchedule/${root.beanName}TransferHandler.java
package com.tuandai.common.taskSchedule;

import com.tuandai.ms.apiutils.exception.AppBusinessException;
import com.tuandai.ms.common.xxljob.AbstractJobHandler;
import com.tuandai.worker.${root.beanName}TransferWorker;
import com.xxl.job.core.handler.annotation.JobHander;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
* Created by ${root.systemUserName}
*/
@JobHander(value = "${root.beanName?uncap_first}Handler")
@Service
public class ${root.beanName}TransferHandler extends AbstractJobHandler {
    @Autowired
    private ${root.beanName}TransferWorker ${root.beanName?uncap_first}TransferWorker;

    @Override
    protected void doExecute(String[] params) throws SQLException {
        if(!${root.beanName?uncap_first}TransferWorker.doTransfer()) {
            throw new AppBusinessException("${root.beanName?uncap_first}TransferHandler运行发生错误");
        }
    }

    @Override
    protected boolean needZkLock() {
        return true;
    }
}
