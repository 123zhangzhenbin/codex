###out:src/main/java/com/tuandai/worker/${root.beanName}TransferWorker.java
package com.tuandai.worker;

import com.tuandai.common.constants.TableNames;
import com.tuandai.service.Cdc${root.beanName}Service;
import com.tuandai.service.intf.CdcService;
import com.tuandai.service.job.JobService;
import com.tuandai.service.job.NoShardingJobSerive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
* Created by huangwenchang on 2017/11/22.
*/
@Component("${root.beanName?uncap_first}TransferWorker")
public class ${root.beanName}TransferWorker extends AbstractWorker {
    @Autowired
    private Cdc${root.beanName}Service cdc${root.beanName}Service;

    @Override
    protected String getTableName() {
        return TableNames.${root.tableName?upper_case};
    }

    @Override
    protected CdcService getCdcService() {
        return cdc${root.beanName}Service;
    }

    @Override
    protected JobService getJobService() {
        return new NoShardingJobSerive();
    }
}
