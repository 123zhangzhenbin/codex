###out:src/test/java/com/tuandai/cdc/Cdc${root.beanName}ServiceTest.java
package com.tuandai.cdc;

import com.tuandai.service.Cdc${root.beanName}Service;
import com.tuandai.config.DataSourceNames;
import com.tuandai.domain.CdcInfo;
import com.tuandai.dto.CdcInfoSearchDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.List;

/**
* Created by ${root.systemUserName}
*/
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class Cdc${root.beanName}ServiceTest {

    private Logger logger = LoggerFactory.getLogger(Cdc${root.beanName}ServiceTest.class);

    @Autowired
    private Cdc${root.beanName}Service cdc${root.beanName}Service;

    @Test
    public void getFirstLsnTest() {
        String lsn = cdc${root.beanName}Service.getFirstLsn(DataSourceNames.RDS_CDC_HT);
        System.out.println(lsn);
    }

    @Test
    public void listInfoTest() {
        CdcInfoSearchDTO cdcInfoSearchDTO = CdcInfoSearchDTO.create(1,  false)
            .mainCondition("0000045400024DA80004", false, null);
        cdcInfoSearchDTO.setQueryDate(true);
        List<CdcInfo> cdcInfos = cdc${root.beanName}Service.listInfo(cdcInfoSearchDTO, DataSourceNames.RDS_CDC_HT);
        cdcInfos.stream().forEach(o -> logger.info(o.toString()));
    }


    @Test
    public void countByLsnTest() {
        logger.info(cdc${root.beanName}Service.getFirstLsn(DataSourceNames.RDS_CDC_HT));
    }
}
