###out:src/main/java/com/tuandai/service/Cdc${root.beanName}Service.java
package com.tuandai.service;

import com.tuandai.dao.CdcDAO;
import com.tuandai.dao.Cdc${root.beanName}DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* Created by ${root.systemUserName}
*/
@Service
public class Cdc${root.beanName}Service extends CdcAbstractService {

    @Autowired
    private Cdc${root.beanName}DAO cdc${root.beanName}DAO;

    @Override
    protected CdcDAO getCdcDAO() {
        return cdc${root.beanName}DAO;
    }
}
