###out:src/main/java/com/tuandai/dao/Cdc${root.beanName}DAO.java
package com.tuandai.dao;

import org.apache.ibatis.annotations.Mapper;

/**
* Created by ${root.systemUserName}
*/
@Mapper
public interface Cdc${root.beanName}DAO extends CdcDAO {

}
