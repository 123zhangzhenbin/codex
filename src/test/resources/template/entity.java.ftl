###out:src/main/java/com/tuandai/ms/subscribe/domain/${root.beanName}.java
package com.tuandai.ms.subscribe.domain;

import java.util.Date;
import java.math.BigDecimal;


/**
* ${root.tableComment} 实体类
*/
public class ${root.beanName} {
    <#list root.tableColumns as column>
    private ${column.propertyType} ${column.propertyName};  // ${column.columnComment!}
    </#list>

    /*----------------------get set 方法----------------------*/
    <#list root.tableColumns as column>
    public ${column.propertyType} get${(column.propertyName)?cap_first}() {return ${column.propertyName};}
    public void set${(column.propertyName)?cap_first}(${column.propertyType} ${column.propertyName}){<#if column.propertyType == 'String'>this.${column.propertyName} = ${column.propertyName} == null ? null : ${column.propertyName}.trim();<#else >this.${column.propertyName} = ${column.propertyName};</#if>}
    </#list>
    /*----------------------toString 方法----------------------*/
    @Override
    public String toString() {
        return "${root.beanName}{" +
        <#list root.tableColumns as column>
        "${column.propertyName}='" + get${column.propertyName?cap_first}() + '\'' + <#if column_has_next>',' + </#if>
        </#list>
        '}';
    }
}