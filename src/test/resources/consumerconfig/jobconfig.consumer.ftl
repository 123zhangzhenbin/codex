###out:jobconfig/consumer/${config.dbName}/${root.tableName}.txt
{
    "columns":[
<#list root.tableColumns as column>
        {
            "columnName":"${column.columnName?lower_case}",
            "columnSize":<#if column.propertyType == 'String' >${column.maxLength}<#elseif column.propertyType == 'BigDecimal' || column.propertyType == 'Float' || column.propertyType == 'Double'>${column.precision}<#else >0</#if>,
            "decimalDigits":<#if column.propertyType == 'BigDecimal' || column.propertyType == 'Float' || column.propertyType == 'Double'>${column.scale}<#else >0</#if>,
            "typeName":"${column.jdbcType}",
            "dataType": ${column.jdbcVendorTypeNumber}
        }<#if column_has_next>,</#if>
</#list>
    ],
    "partition": ${projectMeta.index},
    "start": 0,
    "tableName": "${root.tableName?lower_case}",
    "targetTableName": "${root.tableName?lower_case}"
},