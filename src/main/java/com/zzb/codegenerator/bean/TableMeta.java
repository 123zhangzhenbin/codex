package com.zzb.codegenerator.bean;

import com.zzb.codegenerator.config.GeneratorConfig;

import java.util.Date;
import java.util.List;

/**
 * 表
 * Created by zzb on 17-3-19.
 */
public class TableMeta {
    private GeneratorConfig config;
    private String tableName;
    private String tableComment;
    private String beanName;
    private String primaryKeyName;
    private List<TableColumn> primaryKeyColumns;
    private List<TableColumn> tableColumns;
    private Date currentTime;
    private String systemUserName;

    public Date getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Date currentTime) {
        this.currentTime = currentTime;
    }

    public String getSystemUserName() {
        return systemUserName;
    }

    public void setSystemUserName(String systemUserName) {
        this.systemUserName = systemUserName;
    }

    public List<TableColumn> getPrimaryKeyColumns() {
        return primaryKeyColumns;
    }

    public void setPrimaryKeyColumns(List<TableColumn> primaryKeyColumns) {
        this.primaryKeyColumns = primaryKeyColumns;
    }

    public GeneratorConfig getConfig() {
        return config;
    }

    public void setConfig(GeneratorConfig config) {
        this.config = config;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPrimaryKeyName() {
        return primaryKeyName;
    }

    public void setPrimaryKeyName(String primaryKeyName) {
        this.primaryKeyName = primaryKeyName;
    }

    public List<TableColumn> getTableColumns() {
        return tableColumns;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public void setTableColumns(List<TableColumn> tableColumns) {
        this.tableColumns = tableColumns;
    }

    @Override
    public String toString() {
        return "TableMeta{" +
                "config=" + config +
                ", tableName='" + tableName + '\'' +
                ", tableComment='" + tableComment + '\'' +
                ", beanName='" + beanName + '\'' +
                ", primaryKeyName='" + primaryKeyName + '\'' +
                ", primaryKeyColumns=" + primaryKeyColumns +
                ", tableColumns=" + tableColumns +
                '}';
    }
}
