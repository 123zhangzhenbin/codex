package com.zzb.codegenerator.bean;

import com.zzb.codegenerator.config.GeneratorConfig;

import java.util.List;

/**
 * 项目根model
 * Created by zzb on 17-3-19.
 */
public class ProjectMeta {
    private int index;
    private TableMeta tableMeta;      //表元素
    private GeneratorConfig generatorConfig;    //配置

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public TableMeta getTableMeta() {
        return tableMeta;
    }

    public void setTableMeta(TableMeta tableMeta) {
        this.tableMeta = tableMeta;
    }

    public GeneratorConfig getGeneratorConfig() {
        return generatorConfig;
    }

    public void setGeneratorConfig(GeneratorConfig generatorConfig) {
        this.generatorConfig = generatorConfig;
    }
}
