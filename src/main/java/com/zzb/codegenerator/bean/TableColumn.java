package com.zzb.codegenerator.bean;

import com.zzb.codegenerator.utils.RandomValueUtils;

import java.sql.JDBCType;

/**
 * 字段
 * Created by zzb on 17-3-19.
 */
public class TableColumn {
    private String columnName;          //字段名
    private String propertyName;        //项目名
    private String columnType;          //字段类型
    private String columnTypeFullName;        //字段类型全名
    private String jdbcType;            //jdbc字段类型
    private Integer jdbcVendorTypeNumber;      //枚举JDBCType的值
    private String propertyType;        //java属性类型
    private String propertyFullType;    //java属性类型全称
    private String columnComment;       //字段注释
    private Boolean primaryKey;         //主键
    private Boolean nullable;            //是否非空
    private Boolean unsigned;           //是否非负
    private String defaultValue;        //字段默认值
    private Boolean ref;                //是否是关联字段
    private String refTable;            //关联表
    private String refColumn;           //关联字段
    private Long maxLength;     //字符串类型最大长度
    private Long precision;     //数字长度
    private Long scale;         //数字精度
    private String randomValue;     //随机数值
    private String randomValueFinal;    //固定随机数值

    public String getRandomValueFinal() {
        if(randomValueFinal == null){
            randomValueFinal = RandomValueUtils.randomValue(columnType,propertyFullType,maxLength,precision,scale);
        }
        return randomValueFinal;
    }

    public void setRandomValueFinal(String randomValueFinal) {
        this.randomValueFinal = randomValueFinal;
    }

    public Integer getJdbcVendorTypeNumber() {
        return jdbcType == null ? 0 : JDBCType.valueOf(jdbcType).getVendorTypeNumber();
    }

    public void setJdbcVendorTypeNumber(Integer jdbcVendorTypeNumber) {
        this.jdbcVendorTypeNumber = jdbcVendorTypeNumber;
    }

    public String getRandomValue() {
        return RandomValueUtils.randomValue(columnType,propertyFullType,maxLength,precision,scale);
    }

    public void setRandomValue(String randomValue) {
        this.randomValue = randomValue;
    }

    public String getColumnTypeFullName() {
        return columnTypeFullName;
    }

    public void setColumnTypeFullName(String columnTypeFullName) {
        this.columnTypeFullName = columnTypeFullName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Long getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Long maxLength) {
        this.maxLength = maxLength;
    }

    public Long getPrecision() {
        return precision;
    }

    public void setPrecision(Long precision) {
        this.precision = precision;
    }

    public Long getScale() {
        return scale;
    }

    public void setScale(Long scale) {
        this.scale = scale;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyFullType() {
        return propertyFullType;
    }

    public void setPropertyFullType(String propertyFullType) {
        this.propertyFullType = propertyFullType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public Boolean getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Boolean getRef() {
        return ref;
    }

    public void setRef(Boolean ref) {
        this.ref = ref;
    }

    public Boolean getNullable() {
        return nullable;
    }

    public void setNullable(Boolean nullable) {
        this.nullable = nullable;
    }

    public Boolean getUnsigned() {
        return unsigned;
    }

    public void setUnsigned(Boolean unsigned) {
        this.unsigned = unsigned;
    }

    public String getRefTable() {
        return refTable;
    }

    public void setRefTable(String refTable) {
        this.refTable = refTable;
    }

    public String getRefColumn() {
        return refColumn;
    }

    public void setRefColumn(String refColumn) {
        this.refColumn = refColumn;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    @Override
    public String toString() {
        return "TableColumn{" +
                "columnName='" + columnName + '\'' +
                ", propertyName='" + propertyName + '\'' +
                ", columnType='" + columnType + '\'' +
                ", jdbcType='" + jdbcType + '\'' +
                ", propertyType='" + propertyType + '\'' +
                ", propertyFullType='" + propertyFullType + '\'' +
                ", columnComment='" + columnComment + '\'' +
                ", primaryKey=" + primaryKey +
                ", ref=" + ref +
                ", nullable=" + nullable +
                ", unsigned=" + unsigned +
                ", refTable='" + refTable + '\'' +
                ", refColumn='" + refColumn + '\'' +
                '}';
    }

}
