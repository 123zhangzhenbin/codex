package com.zzb.codegenerator.utils;

import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 工具类 Created by zzb on 15/12/30.
 */
public class Misc {
	public static boolean isZero(Long i) {
		return i == null || i.equals(0L);
	}

	public static boolean isZero(Integer i) {
		return i == null || i.equals(0);
	}

	public static boolean isZero(Float f) {
		return f == null || f.equals(0f);
	}

	public static boolean isZero(Double d) {
		return d == null || d.equals(0d);
	}

	public static boolean isNotZero(Long i) {
		return !isZero(i);
	}

	public static boolean isNotZero(Integer i) {
		return !isZero(i);
	}

	public static boolean isNotZero(Float f) {
		return !isZero(f);
	}

	public static boolean isNotZero(Double d) {
		return !isZero(d);
	}

	public static boolean isTrue(Boolean b) {
		return null != b && b;
	}

	public static boolean isFalse(Boolean b) {
		return !isTrue(b);
	}

	public static int toPrimitive(Integer value, int defaultValue) {
		return value == null ? defaultValue : value;
	}

	public static float toPrimitive(Float value, float defaultValue) {
		return value == null ? defaultValue : value;
	}

	public static double toPrimitive(Double value, double defaultValue) {
		return value == null ? defaultValue : value;
	}

	public static char toPrimitive(Character value, char defaultValue) {
		return value == null ? defaultValue : value;
	}

	public static boolean toPrimitive(Boolean value, boolean defaultValue) {
		return value == null ? defaultValue : value;
	}

	public static short toPrimitive(Short value, short defaultValue) {
		return value == null ? defaultValue : value;
	}

	public static final char UNDERLINE = '_';

	public static String camelToUnderline(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (Character.isUpperCase(c)) {
				sb.append(UNDERLINE);
				sb.append(Character.toLowerCase(c));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public static String underlineToCamel(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (c == UNDERLINE) {
				if (++i < len) {
					sb.append(Character.toUpperCase(param.charAt(i)));
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public static String underlineToCamel2(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		StringBuilder sb = new StringBuilder(param);
		Matcher mc = Pattern.compile("_").matcher(param);
		int i = 0;
		while (mc.find()) {
			int position = mc.end() - (i++);
			// String.valueOf(Character.toUpperCase(sb.charAt(position)));
			sb.replace(position - 1, position + 1,
					sb.substring(position, position + 1).toUpperCase());
		}
		return sb.toString();
	}

	/**获取目录下所有文件名*/
	public static List<String> getSimpleFilenames (String path){
		List<String> filenames = new ArrayList<>();

		File file = new File(path);
		if(file.isDirectory()){
			File[] array = file.listFiles();
			if(ArrayUtils.isNotEmpty(array)){
				for (File anArray : array) {
					if (anArray.isFile()) {
						filenames.add(anArray.getName());
					}
				}
			}
		}
		return filenames;
	}

	public static void main(String[] args) {
		String a_b = "sys_user_information";
		String aB = underlineToCamel(a_b);
		String aB2 = underlineToCamel2(a_b);
		System.out.println(aB);
		System.out.println(aB2);
	}
}
