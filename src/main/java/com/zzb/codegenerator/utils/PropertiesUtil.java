package com.zzb.codegenerator.utils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 获取配置文件的工具类
 * Created by zzb on 16/1/22.
 */
public class PropertiesUtil {
    public static Map<String, Properties> propertiesPool;//配置池
    public static String getProperties(String fileName, String key) {
        Properties properties = getProperties(fileName);
        if(properties == null) return "";
        return properties.getProperty(key,"");
    }

    public static Properties getProperties(String fileName){
        if (propertiesPool == null) {
            propertiesPool = new HashMap<>();
        }
        Properties properties = null;
        if (propertiesPool.containsKey(fileName)) {
            properties = propertiesPool.get(fileName);
        } else {
            properties = new Properties();

            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            if (is == null) {
                try {
                    String path = java.net.URLDecoder.decode(PropertiesUtil.class.getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8");
                    if (path.endsWith(".jar")) {// 可执行jar包运行的结果里包含".jar"
                        // 截取路径中的jar包名
                        path = path.substring(0, path.lastIndexOf("/") + 1);
                    }

                    File file = new File(path);
                    path = file.getAbsolutePath();//得到windows下的正确路径
                    path = path + File.separator + fileName;
                    is = new FileInputStream(path);
                } catch (IOException e) {
                    System.err.println("没有找到["+fileName+"]文件");
                }
            }

            if (is == null) {
                return null;
            }
            else {
                try {
                    properties.load(is);
                    is.close();

                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
                propertiesPool.put(fileName,properties);
            }
        }
        return properties;
    }

    /**
     * 载入配置map
     * @param fileName 载入所有配置并放入map
     * @return 配置文件map
     */
    public static Map<String,String> loadAllProperties(String fileName){
        return mergeProperties(new HashMap<>(),fileName,true);
    }

    /**
     * 合并配置项
     * @param originMap 原始map
     * @param fileName 文件名
     * @param latest 发生冲突时是否使用新文件配置
     * @return 合并后的配置文件map
     */
    public static Map<String , String > mergeProperties(Map<String,String> originMap ,  String fileName,boolean latest){
        if(originMap == null) originMap = new HashMap<>();
        Properties properties = getProperties(fileName);
        if(properties == null) return originMap;

        Set<String> propertiesKeys = properties.stringPropertyNames();
        //如果map中不包含配置项或者配置为始终使用最新项，则更新配置map
        for (String propertiesKey : propertiesKeys) {
            if (!originMap.containsKey(propertiesKey) || latest)
                originMap.put(propertiesKey,properties.getProperty(propertiesKey));
        }

        return originMap;
    }
}
