package com.zzb.codegenerator.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by zhangzhenbin on 18-1-12.
 */
public class RandomValueUtils {
    public static String randomValue(String columnType,String propertyFullType,Long maxLength,Long precision , Long scale ){
        String v = null;
        switch (propertyFullType){
            case "java.lang.String" :
                if(maxLength == null  || maxLength == 1){
                    v = "1";
                } else if( maxLength>= 72 ||  "UNIQUEIDENTIFIER".equals(columnType) || maxLength == -1){
                    v = UUID.randomUUID().toString();
                } else {
                    v = UUID.randomUUID().toString().substring(0,maxLength.intValue()/2);
                }
                break;
            case "java.lang.Integer":
            case "java.lang.Long":
                int i = new Double(Math.random() * 100).intValue();
                v = "" + i; break;
            case "java.lang.Float":
            case "java.lang.Double":
            case "java.math.BigDecimal":
                v  = new BigDecimal(Math.random() * 100).setScale(scale == null?2:scale.intValue(), RoundingMode.HALF_UP).toString();break;
            case "java.lang.Boolean":
                boolean b = new Double(Math.random() * 100).intValue()%2 == 1;
                v = b?"true":"false";break;
            case "java.util.Date":
            case "java.sql.Date":
                v = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());break;
            default:
                System.err.println("不支持的类型["+propertyFullType+"]");
        }
        return v;
    }
}
