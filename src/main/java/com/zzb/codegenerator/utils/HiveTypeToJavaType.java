package com.zzb.codegenerator.utils;

import java.sql.JDBCType;
import java.util.HashMap;
import java.util.Map;

import static java.sql.JDBCType.*;

/**
 * Created by zhangzhenbin on 18-1-10.
 */
public class HiveTypeToJavaType {
    public static Map<String, HiveTypeToJavaType> hive2Java = new HashMap<>();

    static {
        hive2Java.put("int", new HiveTypeToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        hive2Java.put("int8", new HiveTypeToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        hive2Java.put("int16", new HiveTypeToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        hive2Java.put("int32", new HiveTypeToJavaType("Integer",
                "java.lang.Long", BIGINT));
        hive2Java.put("bigint", new HiveTypeToJavaType("Long",
                "java.lang.Long", BIGINT));
        hive2Java.put("string", new HiveTypeToJavaType("String",
                "java.lang.String", VARCHAR));
        hive2Java.put("double", new HiveTypeToJavaType("Double",
                "java.lang.Double", DOUBLE));
        hive2Java.put("float", new HiveTypeToJavaType("Float",
                "java.lang.Float", FLOAT));
        hive2Java.put("blob", new HiveTypeToJavaType("Byte[]",
                "java.lang.Byte[]", BLOB));
        hive2Java.put("binary", new HiveTypeToJavaType("Byte[]",
                "java.lang.Byte[]", VARBINARY));

        hive2Java.put("bit", new HiveTypeToJavaType("Boolean",
                "java.lang.Boolean", BIT));
        hive2Java.put("boolean", new HiveTypeToJavaType("Boolean",
                "java.lang.Boolean", BOOLEAN));
        hive2Java.put("bool", new HiveTypeToJavaType("Boolean",
                "java.lang.Boolean", BOOLEAN));

        hive2Java.put("timestamp", new HiveTypeToJavaType("Date",
                "java.util.Date", TIMESTAMP));
        hive2Java.put("unixtime_micros", new HiveTypeToJavaType("Date",
                "java.util.Date", TIMESTAMP));

        hive2Java.put("DECIMAL", new HiveTypeToJavaType("BigDecimal",
                "java.math.BigDecimal", DECIMAL));
    }

    private String javaType;
    private String javaFullType;
    private JDBCType sqlserverType;

    public HiveTypeToJavaType(String javaType, String javaFullType,
                              JDBCType sqlserverType) {
        this.javaType = javaType;
        this.javaFullType = javaFullType;
        this.sqlserverType = sqlserverType;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getJavaFullType() {
        return javaFullType;
    }

    public void setJavaFullType(String javaFullType) {
        this.javaFullType = javaFullType;
    }

    public JDBCType getSqlserverType() {
        return sqlserverType;
    }

    public void setSqlserverType(JDBCType sqlserverType) {
        this.sqlserverType = sqlserverType;
    }
}
