package com.zzb.codegenerator.utils;

import java.sql.JDBCType;
import java.util.HashMap;
import java.util.Map;
import static java.sql.JDBCType.*;
/**
 * 类型对应 Created by 瑱彬 on 2016/5/1.
 */
public final class MySQLTypeToJavaType {
	private String javaType;
	private String javaFullType;
	private JDBCType mysqlType;

	public MySQLTypeToJavaType(String javaType, String javaFullType,
							   JDBCType mysqlType) {
		this.javaType = javaType;
		this.javaFullType = javaFullType;
		this.mysqlType = mysqlType;
	}

	public static Map<String, MySQLTypeToJavaType> mysql2java = new HashMap<>();
	static {
		mysql2java.put("INT", new MySQLTypeToJavaType("Integer",
				"java.lang.Integer", INTEGER));
		mysql2java.put("INTEGER", new MySQLTypeToJavaType("Integer",
				"java.lang.Integer", INTEGER));
		mysql2java.put("TINYINT", new MySQLTypeToJavaType("Integer",
				"java.lang.Integer", TINYINT));
		mysql2java.put("SMALLINT", new MySQLTypeToJavaType("Integer",
				"java.lang.Integer", SMALLINT));
		mysql2java.put("MEDIUMINT", new MySQLTypeToJavaType("Integer",
				"java.lang.Integer", INTEGER));

		mysql2java.put("VARCHAR", new MySQLTypeToJavaType("String",
				"java.lang.String", VARCHAR));
		mysql2java.put("TINYBLOB", new MySQLTypeToJavaType("String",
				"java.lang.String", BLOB));
		mysql2java.put("TEXT", new MySQLTypeToJavaType("String",
				"java.lang.String", VARCHAR));
		mysql2java.put("MEDIUMTEXT", new MySQLTypeToJavaType("String",
				"java.lang.String", VARCHAR));
		mysql2java.put("LONGTEXT", new MySQLTypeToJavaType("String",
				"java.lang.String", VARCHAR));
		mysql2java.put("MEDIUMBLOB", new MySQLTypeToJavaType("String",
				"java.lang.String", BLOB));
		mysql2java.put("CHAR", new MySQLTypeToJavaType("String",
				"java.lang.String", CHAR));

		mysql2java.put("BIGINT", new MySQLTypeToJavaType("Long",
				"java.lang.Long", BIGINT));

		mysql2java.put("DOUBLE", new MySQLTypeToJavaType("Double",
				"java.lang.Double", DOUBLE));

		mysql2java.put("FLOAT", new MySQLTypeToJavaType("Float",
				"java.lang.Float", FLOAT));

		mysql2java.put("BLOB", new MySQLTypeToJavaType("Byte[]",
				"java.lang.Byte[]", BLOB));

		mysql2java.put("BIT", new MySQLTypeToJavaType("Boolean",
				"java.lang.Boolean", BIT));
		mysql2java.put("BOOLEAN", new MySQLTypeToJavaType("Boolean",
				"java.lang.Boolean", BOOLEAN));

		mysql2java.put("TIMESTAMP", new MySQLTypeToJavaType("Date",
				"java.util.Date", TIMESTAMP));
		mysql2java.put("DATETIME", new MySQLTypeToJavaType("Date",
				"java.util.Date", TIMESTAMP));
		mysql2java.put("TIME", new MySQLTypeToJavaType("Date",
				"java.util.Date", TIME));
		mysql2java.put("DATE", new MySQLTypeToJavaType("Date",
				"java.util.Date", DATE));
		mysql2java.put("DECIMAL", new MySQLTypeToJavaType("BigDecimal",
				"java.math.BigDecimal", DECIMAL));
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getJavaFullType() {
		return javaFullType;
	}

	public void setJavaFullType(String javaFullType) {
		this.javaFullType = javaFullType;
	}

	public JDBCType getMysqlType() {
		return mysqlType;
	}

	public void setMysqlType(JDBCType mysqlType) {
		this.mysqlType = mysqlType;
	}
}
