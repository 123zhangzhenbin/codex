package com.zzb.codegenerator.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by ZZB on 2017/9/3.
 */
public class OperatorEnvUtils {
    public static String getSystemUserName(){
        String userName = null;
        try {
            userName = System.getProperty("user.name");
        } catch (Exception e) {
            userName = "获取异常";
        }
        if(StringUtils.isEmpty(userName)){
            userName = "匿名";
        }
        return userName;
    }
}
