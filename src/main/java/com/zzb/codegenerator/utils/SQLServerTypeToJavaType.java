package com.zzb.codegenerator.utils;

import java.sql.JDBCType;
import java.util.HashMap;
import java.util.Map;

import static java.sql.JDBCType.*;

/**
 * Created by zhangzhenbin on 18-1-10.
 */
public class SQLServerTypeToJavaType {
    private String javaType;
    private String javaFullType;
    private JDBCType sqlserverType;

    public SQLServerTypeToJavaType(String javaType, String javaFullType,
                                   JDBCType sqlserverType) {
        this.javaType = javaType;
        this.javaFullType = javaFullType;
        this.sqlserverType = sqlserverType;
    }

    public static Map<String, SQLServerTypeToJavaType> sqlServer2Java = new HashMap<>();
    static {
        sqlServer2Java.put("INT", new SQLServerTypeToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        sqlServer2Java.put("INTEGER", new SQLServerTypeToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        sqlServer2Java.put("TINYINT", new SQLServerTypeToJavaType("Integer",
                "java.lang.Integer", TINYINT));
        sqlServer2Java.put("SMALLINT", new SQLServerTypeToJavaType("Integer",
                "java.lang.Integer", SMALLINT));
        sqlServer2Java.put("MEDIUMINT", new SQLServerTypeToJavaType("Integer",
                "java.lang.Integer", INTEGER));

        sqlServer2Java.put("VARCHAR", new SQLServerTypeToJavaType("String",
                "java.lang.String", VARCHAR));
        sqlServer2Java.put("CHAR", new SQLServerTypeToJavaType("String",
                "java.lang.String", CHAR));
        sqlServer2Java.put("NVARCHAR", new SQLServerTypeToJavaType("String",
                "java.lang.String", NVARCHAR));
        sqlServer2Java.put("NCHAR", new SQLServerTypeToJavaType("String",
                "java.lang.String", NCHAR));
        sqlServer2Java.put("UNIQUEIDENTIFIER", new SQLServerTypeToJavaType("String",
                "java.lang.String", VARCHAR));
        sqlServer2Java.put("TEXT", new SQLServerTypeToJavaType("String",
                "java.lang.String", LONGVARCHAR));
        sqlServer2Java.put("SYSNAME", new SQLServerTypeToJavaType("String",
                "java.lang.String", VARCHAR));
        sqlServer2Java.put("NTEXT", new SQLServerTypeToJavaType("String",
                "java.lang.String", LONGVARCHAR));
        sqlServer2Java.put("CHAR", new SQLServerTypeToJavaType("String",
                "java.lang.String", CHAR));

        sqlServer2Java.put("BIGINT", new SQLServerTypeToJavaType("Long",
                "java.lang.Long", BIGINT));

        sqlServer2Java.put("DOUBLE", new SQLServerTypeToJavaType("Double",
                "java.lang.Double", DOUBLE));
        sqlServer2Java.put("REAL", new SQLServerTypeToJavaType("Double",
                "java.lang.Double", DOUBLE));

        sqlServer2Java.put("FLOAT", new SQLServerTypeToJavaType("Float",
                "java.lang.Float", FLOAT));

        sqlServer2Java.put("BLOB", new SQLServerTypeToJavaType("Byte[]",
                "java.lang.Byte[]", BLOB));
        sqlServer2Java.put("TINYBLOB", new SQLServerTypeToJavaType("String",
                "java.lang.String", BLOB));
        sqlServer2Java.put("MEDIUMBLOB", new SQLServerTypeToJavaType("String",
                "java.lang.String", BLOB));
        sqlServer2Java.put("BINARY", new SQLServerTypeToJavaType("Byte[]",
                "java.lang.Byte[]", BINARY));
        sqlServer2Java.put("VARBINARY", new SQLServerTypeToJavaType("Byte[]",
                "java.lang.Byte[]", VARBINARY));

        sqlServer2Java.put("BIT", new SQLServerTypeToJavaType("Boolean",
                "java.lang.Boolean", BIT));
        sqlServer2Java.put("BOOLEAN", new SQLServerTypeToJavaType("Boolean",
                "java.lang.Boolean", BOOLEAN));

        sqlServer2Java.put("TIMESTAMP", new SQLServerTypeToJavaType("Date",
                "java.util.Date", TIMESTAMP));
        sqlServer2Java.put("DATETIME", new SQLServerTypeToJavaType("Date",
                "java.util.Date", TIMESTAMP));
        sqlServer2Java.put("DATETIME2", new SQLServerTypeToJavaType("Date",
                "java.util.Date", TIMESTAMP));
        sqlServer2Java.put("TIME", new SQLServerTypeToJavaType("Date",
                "java.util.Date", TIME));
        sqlServer2Java.put("DATE", new SQLServerTypeToJavaType("Date",
                "java.util.Date", DATE));

        sqlServer2Java.put("DECIMAL", new SQLServerTypeToJavaType("BigDecimal",
                "java.math.BigDecimal", DECIMAL));
        sqlServer2Java.put("NUMERIC", new SQLServerTypeToJavaType("BigDecimal",
                "java.math.BigDecimal", DECIMAL));
        sqlServer2Java.put("MONEY", new SQLServerTypeToJavaType("BigDecimal",
                "java.math.BigDecimal", DECIMAL));
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getJavaFullType() {
        return javaFullType;
    }

    public void setJavaFullType(String javaFullType) {
        this.javaFullType = javaFullType;
    }

    public JDBCType getSqlserverType() {
        return sqlserverType;
    }

    public void setSqlserverType(JDBCType sqlserverType) {
        this.sqlserverType = sqlserverType;
    }
}
