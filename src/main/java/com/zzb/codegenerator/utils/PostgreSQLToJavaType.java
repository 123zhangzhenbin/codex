package com.zzb.codegenerator.utils;


import java.sql.JDBCType;
import java.util.HashMap;
import java.util.Map;
import static java.sql.JDBCType.*;

/**
 * 数据库表的类型转换
 * Created by zhangzhenbin on 17-8-18.
 */
public class PostgreSQLToJavaType {
    private String javaType;
    private String javaFullType;
    private JDBCType postgresType;

    public PostgreSQLToJavaType(String javaType, String javaFullType,
                                JDBCType postgresType) {
        this.javaType = javaType;
        this.javaFullType = javaFullType;
        this.postgresType = postgresType;
    }

    public static Map<String, PostgreSQLToJavaType> postgresToJava = new HashMap<>();
    static {
        postgresToJava.put("int2", new PostgreSQLToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        postgresToJava.put("int4", new PostgreSQLToJavaType("Integer",
                "java.lang.Integer", INTEGER));
        postgresToJava.put("int8", new PostgreSQLToJavaType("Long",
                "java.lang.Integer", BIGINT));
        postgresToJava.put("json", new PostgreSQLToJavaType("String",
                "java.lang.String", VARCHAR));
        postgresToJava.put("text", new PostgreSQLToJavaType("String",
                "java.lang.String", VARCHAR));
        postgresToJava.put("varchar", new PostgreSQLToJavaType("String",
                "java.lang.String", VARCHAR));
        postgresToJava.put("char", new PostgreSQLToJavaType("String",
                "java.lang.String", VARCHAR));
        postgresToJava.put("bpchar", new PostgreSQLToJavaType("String",
                "java.lang.String", VARCHAR));

        postgresToJava.put("float8", new PostgreSQLToJavaType("Double",
                "java.lang.Double", DOUBLE));

        postgresToJava.put("float4", new PostgreSQLToJavaType("Float",
                "java.lang.Float", FLOAT));

        postgresToJava.put("bool", new PostgreSQLToJavaType("Boolean",
                "java.lang.Boolean", BOOLEAN));

        postgresToJava.put("timestamp", new PostgreSQLToJavaType("Date",
                "java.util.Date", DATE));
        postgresToJava.put("date", new PostgreSQLToJavaType("Date",
                "java.util.Date", DATE));

        postgresToJava.put("numeric", new PostgreSQLToJavaType("BigDecimal",
                "java.math.BigDecimal", DECIMAL));
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getJavaFullType() {
        return javaFullType;
    }

    public void setJavaFullType(String javaFullType) {
        this.javaFullType = javaFullType;
    }

    public JDBCType getPostgresType() {
        return postgresType;
    }

    public void setPostgresType(JDBCType postgresType) {
        this.postgresType = postgresType;
    }
}
