package com.zzb.codegenerator.config;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * 配置类
 * Created by zhangzhenbin on 2016/10/7.
 */
public class GeneratorConfig {
    private String projectName;         //项目名称
    private String projectSummary;      //项目显示名称
    private String projectDirectory;    //项目存放路径
    private String basePackageName;     //基础包名，如com.zzb.codegenerator
    private String modulePackageName;   //模块包名，附加在基础包名后，如果没有则代码直接创建在基础包下
    private String entityPackageName;   //实体类包放在什么地方
    private String daoPackageName;      //DAO包放在什么地方
    private String jpaPackageName;      //jpa放在什么地方
    private String mybatisPackageName;  //mybatis放在什么地方
    private String servicePackageName;  //service放在什么地方
    private String voPackageName;       //vo放在什么地方
    private String formPackageName;     //form放在什么地方
    private String mapperXMLDirectoryName; //mapper xml文件放在什么地方
    private String htmlDirectoryName;     //html页面放在什么地方
    private String cssDirectoryName;      //css放在什么地方
    private String jsDirectoryName;         //js放在什么地方
    private String controllerPackageName;   //控制层包名，如果没有则直接在模块下
    private String apiPackageName;          //api接口放在什么地方
    private String jdbcUrl;             //jdbc地址
    private String dbName;              //数据库名
    private String schema;              //数据库名
    private String jdbcUsername;        //用户名
    private String jdbcPassword;        //密码
    private String tableNames;          //表名
    private String filterColumns;       //过滤字段
    private String beanNames;           //实体名
    private String nameType;            //命名方式，HUMP驼峰，UNDERLINE下划线，NATIVE原生
    private String templateDir;         //模板存放位置
    private String saveDir;             //文件输出存放位置
    private String outFilePattern="###out:";      //输出文件的正则匹配
    private String templateType;        //模板方式
    private String primaryKeys;        //主键列表


    public String getPrimaryKeys() {
        return primaryKeys;
    }

    public void setPrimaryKeys(String primaryKeys) {
        this.primaryKeys = primaryKeys;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getFilterColumns() {
        return filterColumns;
    }

    public void setFilterColumns(String filterColumns) {
        this.filterColumns = filterColumns;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    //拼接包名和模块名
    public String getPackageName(){
        if(StringUtils.isNotBlank(modulePackageName )){
            return basePackageName + "."+ modulePackageName;
        }
        return basePackageName;
    }
    //拼接controller包名和模块名
    public String getFullControllerPackageName(){
        if(StringUtils.isNotBlank(modulePackageName)){
            return controllerPackageName + "." + modulePackageName;
        }
        return controllerPackageName;
    }
    //拼接api包名和模块名
    public String getFullApiPackageName(){
        if(StringUtils.isNotBlank(modulePackageName)){
            return apiPackageName + "." + modulePackageName;
        }
        return apiPackageName;
    }

    public String getSavePath(){
        return saveDir;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getBasePackageName() {
        return basePackageName;
    }

    public void setBasePackageName(String basePackageName) {
        this.basePackageName = basePackageName;
    }

    public String getModulePackageName() {
        return modulePackageName;
    }

    public void setModulePackageName(String modulePackageName) {
        this.modulePackageName = modulePackageName;
    }

    public String getControllerPackageName() {
        return controllerPackageName;
    }

    public void setControllerPackageName(String controllerPackageName) {
        this.controllerPackageName = controllerPackageName;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getJdbcUsername() {
        return jdbcUsername;
    }

    public void setJdbcUsername(String jdbcUsername) {
        this.jdbcUsername = jdbcUsername;
    }

    public String getJdbcPassword() {
        return jdbcPassword;
    }

    public void setJdbcPassword(String jdbcPassword) {
        this.jdbcPassword = jdbcPassword;
    }

    public String getTableNames() {
        return tableNames;
    }

    public void setTableNames(String tableNames) {
        this.tableNames = tableNames;
    }

    public String getBeanNames() {
        return beanNames;
    }

    public void setBeanNames(String beanNames) {
        this.beanNames = beanNames;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getTemplateDir() {
        return templateDir;
    }

    public void setTemplateDir(String templateDir) {
        this.templateDir = templateDir;
    }

    public String getSaveDir() {
        return saveDir;
    }

    public void setSaveDir(String saveDir) {
        this.saveDir = saveDir;
    }

    public String getProjectSummary() {
        return projectSummary;
    }

    public void setProjectSummary(String projectSummary) {
        this.projectSummary = projectSummary;
    }

    public String getProjectDirectory() {
        return projectDirectory;
    }

    public void setProjectDirectory(String projectDirectory) {
        this.projectDirectory = projectDirectory;
    }

    public String getEntityPackageName() {
        return entityPackageName;
    }

    public void setEntityPackageName(String entityPackageName) {
        this.entityPackageName = entityPackageName;
    }

    public String getDaoPackageName() {
        return daoPackageName;
    }

    public void setDaoPackageName(String daoPackageName) {
        this.daoPackageName = daoPackageName;
    }

    public String getJpaPackageName() {
        return jpaPackageName;
    }

    public void setJpaPackageName(String jpaPackageName) {
        this.jpaPackageName = jpaPackageName;
    }

    public String getMybatisPackageName() {
        return mybatisPackageName;
    }

    public void setMybatisPackageName(String mybatisPackageName) {
        this.mybatisPackageName = mybatisPackageName;
    }

    public String getServicePackageName() {
        return servicePackageName;
    }

    public void setServicePackageName(String servicePackageName) {
        this.servicePackageName = servicePackageName;
    }

    public String getVoPackageName() {
        return voPackageName;
    }

    public void setVoPackageName(String voPackageName) {
        this.voPackageName = voPackageName;
    }

    public String getFormPackageName() {
        return formPackageName;
    }

    public void setFormPackageName(String formPackageName) {
        this.formPackageName = formPackageName;
    }

    public String getMapperXMLDirectoryName() {
        return mapperXMLDirectoryName;
    }

    public void setMapperXMLDirectoryName(String mapperXMLDirectoryName) {
        this.mapperXMLDirectoryName = mapperXMLDirectoryName;
    }

    public String getHtmlDirectoryName() {
        return htmlDirectoryName;
    }

    public void setHtmlDirectoryName(String htmlDirectoryName) {
        this.htmlDirectoryName = htmlDirectoryName;
    }

    public String getCssDirectoryName() {
        return cssDirectoryName;
    }

    public void setCssDirectoryName(String cssDirectoryName) {
        this.cssDirectoryName = cssDirectoryName;
    }

    public String getJsDirectoryName() {
        return jsDirectoryName;
    }

    public void setJsDirectoryName(String jsDirectoryName) {
        this.jsDirectoryName = jsDirectoryName;
    }

    public String getApiPackageName() {
        return apiPackageName;
    }

    public void setApiPackageName(String apiPackageName) {
        this.apiPackageName = apiPackageName;
    }

    public String getOutFilePattern() {
        return outFilePattern;
    }

    public void setOutFilePattern(String outFilePattern) {
        this.outFilePattern = outFilePattern;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }
}
