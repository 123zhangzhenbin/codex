package com.zzb.codegenerator.config;

/**
 * 配置文件生产接口
 * Created by zhangzhenbin on 2016/10/7.
 */
public interface IGeneratorConfigFactory {
    /**生产配置文件*/
    GeneratorConfig build( String filename);
}
