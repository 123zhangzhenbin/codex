package com.zzb.codegenerator.generator.table;

import com.zzb.codegenerator.bean.ProjectMeta;
import com.zzb.codegenerator.bean.TableColumn;
import com.zzb.codegenerator.bean.TableMeta;
import com.zzb.codegenerator.config.GeneratorConfig;
import com.zzb.codegenerator.generator.connect.ConnectPool;
import com.zzb.codegenerator.generator.context.TableMetaContext;
import com.zzb.codegenerator.generator.dctm.DbToTableMetaConvertor;
import com.zzb.codegenerator.utils.GetJdbcUrlUtil;
import com.zzb.codegenerator.utils.Misc;
import com.zzb.codegenerator.utils.OperatorEnvUtils;
import com.zzb.codegenerator.utils.PostgreSQLToJavaType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by zhangzhenbin on 17-8-24.
 */
public class ITableMetaFoctory {
    private DbToTableMetaConvertor dbToTableMetaConvertor;

    public ITableMetaFoctory(DbToTableMetaConvertor dbToTableMetaConvertor) {
        this.dbToTableMetaConvertor = dbToTableMetaConvertor;
    }

    /**
     * 根据命名规范重命名
     */
    public static String rename(String type, String preStr) {
        String str = preStr;
        switch (type) {
            case "NATIVE":
                str = preStr;
                break;
            case "UNDERLINE":
                str = Misc.camelToUnderline(preStr);
                break;
            case "HUMP":
            default:
                str = Misc.underlineToCamel(preStr);
                break;
        }
        return str;
    }

    public TableMeta createTableMeta(GeneratorConfig config, String tableName) throws Exception {
        TableMeta tableMeta = dbToTableMetaConvertor.convertFromTableName(tableName);
        if (StringUtils.isNotBlank(config.getFilterColumns())) {
            Set<String> filterColumns = Arrays.stream(config.getFilterColumns().split(",")).
                    filter(s -> s.toLowerCase().startsWith(tableName.toLowerCase()))
                    .map(s -> s.split("\\.")[1].toLowerCase()).collect(Collectors.toSet());
            if (!filterColumns.isEmpty())
                tableMeta.setTableColumns(tableMeta.getTableColumns().stream()
                        .filter(tableColumn -> !filterColumns.contains(tableColumn.getColumnName().toLowerCase())).collect(Collectors.toList()));
        }
        return tableMeta;
    }

    /***
     * 获取tableMeta列表
     */
    public List<TableMeta> createTableMetaList(GeneratorConfig config) throws Exception {
        //初始化TableMetaContext
        Map<String, String> tableMap = TableMetaContext.initAndGetTableMap(config);
        List<TableMeta> tableMetas = new ArrayList<>();
        //遍历要生成的表
        for (String table : tableMap.keySet()) {
            //通过metaData和tableName创建表结构POJO
            tableMetas.add(createTableMeta(config, table));
        }
        return tableMetas;
    }

    public List<ProjectMeta> createProjectMetaList(GeneratorConfig config) throws Exception {
        List<TableMeta> tableMetas = createTableMetaList(config);
        final AtomicInteger idx= new AtomicInteger(1);
        return tableMetas.stream().map(tableMeta -> {
            ProjectMeta projectMeta = new ProjectMeta();
            projectMeta.setGeneratorConfig(config);
            projectMeta.setTableMeta(tableMeta);
            projectMeta.setIndex(idx.getAndIncrement());
            return projectMeta;
        }).collect(Collectors.toList());
    }
}
