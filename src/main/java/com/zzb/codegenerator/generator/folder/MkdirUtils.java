package com.zzb.codegenerator.generator.folder;

import com.zzb.codegenerator.config.GeneratorConfig;

import java.io.File;

/**
 * 根据configure创建文件夹
 * Created by zhangzhenbin on 17-4-5.
 */
public class MkdirUtils {
    /**
     * 创建文件夹
     */
    public static boolean makeDir(GeneratorConfig config) {
        String savepath = config.getSaveDir();
        File fsavepath = new File(savepath);
        if (!fsavepath.exists() && !fsavepath.isDirectory()) {
            System.out.println("创建文件夹：+ " + savepath);
            try {
                return fsavepath.mkdirs();
            } catch (Exception e) {
                System.out.println("ERROR-创建文件夹失败");
                e.printStackTrace();
            }
        }
        if (fsavepath.isFile()) {
            System.out.println("ERROR-存储地址不能填文件");
            return false;
        }
        return true;
    }

}
