package com.zzb.codegenerator.generator;

import com.zzb.codegenerator.bean.ProjectMeta;
import com.zzb.codegenerator.bean.TableMeta;
import com.zzb.codegenerator.config.GeneratorConfig;

import java.util.List;

/**
 * 生成器接口
 * Created by zhangzhenbin on 17-4-5.
 */
public interface IGenerator {
    void generator(GeneratorConfig config,List<ProjectMeta> tableMetas);
}
