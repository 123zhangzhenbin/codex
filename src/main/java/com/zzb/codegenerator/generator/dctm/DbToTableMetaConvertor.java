package com.zzb.codegenerator.generator.dctm;


import com.zzb.codegenerator.bean.TableMeta;

import java.sql.Connection;

/**
 * Created by ZZB on 2017/12/4.
 */
public interface DbToTableMetaConvertor {

    TableMeta convertFromTableName(String tableName) throws Exception;
    boolean tableExist(Connection connection, String tableName);
}
