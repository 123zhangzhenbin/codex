package com.zzb.codegenerator.generator.context;

import com.zzb.codegenerator.bean.ProjectMeta;
import com.zzb.codegenerator.config.GeneratorConfig;
import com.zzb.codegenerator.generator.dctm.hive.HiveToTableMetaConvertor;
import com.zzb.codegenerator.generator.dctm.mysql.MysqlDbToTableMetaConvertor;
import com.zzb.codegenerator.generator.dctm.postgresql.PostgresqlDbToTableMetaConvertor;
import com.zzb.codegenerator.generator.dctm.sqlserver.SQLServerToTableMetaConvertor;
import com.zzb.codegenerator.generator.table.ITableMetaFoctory;
import com.zzb.codegenerator.utils.GetJdbcUrlUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TableMeta的上下文
 * Created by zhangzhenbin on 17-4-5.
 */
public class TableMetaContext {
    private static Map<String, String> tableMap = new HashMap<>();
    private static Map<String, String> tablePkMap = new HashMap<>();

    public static Map<String, String> initAndGetTableMap(GeneratorConfig config) {
        //初始化
        initTableMap(config);
        return tableMap;
    }

    public static Map<String, String> getTableMap() {
        return tableMap;
    }

    public static Map<String, String> getTablePkMap(){
        return tablePkMap;
    }

    /**
     * 初始化预构建tableMap
     */
    private static void initTableMap(GeneratorConfig config) {
        if (tableMap == null) tableMap = new HashMap<>();
        if (tablePkMap == null) tablePkMap = new HashMap<>();
        if(StringUtils.isNotBlank(config.getTableNames())){
            String[] tableNames = config.getTableNames().split(",");
            String[] beanNames = config.getBeanNames().split(",");
            if(StringUtils.isNotBlank(config.getPrimaryKeys())){
                String[] primaryKeys = config.getPrimaryKeys().split("&");
                if(!ArrayUtils.isSameLength(tableNames,primaryKeys)){
                    System.err.println("ERROR-配置项primaryKeys和tableNames长度必须对应");
                    return;
                }
                for (int i = 0; i < tableNames.length; i++) {
                    tablePkMap.put(tableNames[i],primaryKeys[i]);
                }
            }
            if(!ArrayUtils.isSameLength(tableNames,beanNames)){
                System.err.println("ERROR-配置项beanNames和tableNames长度必须对应");
                return;
            }
            for (int i = 0; i < tableNames.length; i++) {
                tableMap.put(tableNames[i], beanNames[i]);
            }
        } else {
            String driverClassName = GetJdbcUrlUtil.getDriverName(config.getJdbcUrl());
            String tableSql = null;
            Connection conn = null;
            try {
                if (driverClassName.equals("org.postgresql.Driver")) {
                } else if(driverClassName.equals("net.sourceforge.jtds.jdbc.Driver")){
                } else if(driverClassName.equals("org.apache.hive.jdbc.HiveDriver")){
                } else if(driverClassName.equals("com.mysql.jdbc.Driver")){
                    tableSql = "select TABLE_NAME as table_name from information_schema.TABLES where TABLE_SCHEMA = '"+config.getDbName()+"'";
                    conn = DriverManager.getConnection(config.getJdbcUrl(),config.getJdbcUsername(),config.getJdbcPassword());
                }
                if(conn != null && tableSql != null){
                    Statement statement = conn.createStatement();
                    ResultSet resultSet = statement.executeQuery(tableSql);
                    while (resultSet.next()){
                        String tableName = resultSet.getString("table_name");
                        tableMap.put(tableName,tableName);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
